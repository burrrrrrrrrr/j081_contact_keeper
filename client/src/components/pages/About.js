import React from 'react'

const About = () => {
    return (
        <section>
            <h1> About this App</h1>
            <p className="bg-dark p">This is a full stack React app for keeper contacts</p>
            <p className="bg-dark p"><strong>Version: </strong> 1.0.0</p>
        </section>
    )
}

export default About
