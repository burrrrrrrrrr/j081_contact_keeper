import React, {useContext, useRef, useEffect} from 'react'
import ContactContext from '../../context/contact/contactContext'

export const ContactFilter = () => {
    const contactContext = useContext(ContactContext)
    const text = useRef('')

    const onChange = e => {
       if(text.current.value !== '') {
           contactContext.filterContacts(e.target.value)
       } else {
           contactContext.clearFilter()
       }
    }

    const {filterContects, clearFilter, filtered} = contactContext

    useEffect(() => {
        if(filtered === null) {
            text.current.value = ''
        }
    })

    return (
        <form>
            <input ref={text} type='text' placeholder='Filter Contacts...' onChange={onChange} />
        </form>
    )
}

export default ContactFilter
